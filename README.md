Ce dépôt contient les fichiers nécessaires à la création d'une page de présentation des données numériques utilisées au sein d'un projet accompagné par l'équipe ELAN. 
Il contient : 
* un fichier "tei2html_data.xsl" : fichier xsl permettant de récupérer les informations au sein d'un fichier TEI et de créer une page html de présentation des données numériques.
* un fichier "Modele_page_data.html" : fichier html permettant d'avoir un modèle de ce à quoi ressemble une page de présentation des données numériques. Néanmoins, il est préférable de créer cette page en utilsant le fichier xsl plutôt qu'en modifiant le modèle html.
* le logo de Nakala
