<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* @author Fanny Corsi et AnneGF@CNRS
* @date : 2024
* desc: XSLT permettant d'extraire d'un fichier TEI les informations nécessaires et de créer une page de présentation des données numériques.
* @version : 03-12-2024
**/
-->
<!DOCTYPE tei2html [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:param name="stand_alone" select="true()"/>

    <xsl:output method="xhtml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>


    <xsl:template match="/">
        <xsl:if test="$stand_alone">
            <xsl:text disable-output-escaping="yes">&lt;html xmlns="http://www.w3.org/1999/xhtml" lang="fr"></xsl:text>
            <head>
                <title>Présentation des données numériques</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <style>
                    h1,
                    h2 {
                        text-align: center;
                        padding: 1em;
                    }</style>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body style="margin-bottom: 15px;"></xsl:text>
        </xsl:if>
        <div class="container">
            <h1>
                <xsl:text>Présentation des données numériques</xsl:text>
            </h1>
            <div>
                <h2>
                    <xsl:text>Le projet et ses données</xsl:text>
                </h2>
                <p>
                    <xsl:text>Ce site est publié par : </xsl:text>
                    <xsl:for-each select="//tei:teiHeader[.//tei:publicationStmt/tei:authority]">
                        <xsl:if test="position() = 1">
                            <xsl:for-each select=".//tei:publicationStmt/tei:authority">
                                <xsl:choose>
                                    <xsl:when test="position() > 1 and position() &lt; last()">
                                        <xsl:text>, </xsl:text>
                                    </xsl:when>
                                    <xsl:when test="position() > 1 and last()">
                                        <xsl:text> et </xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                                <xsl:choose>
                                    <xsl:when test="@sameAs">
                                        <xsl:variable name="id"
                                            select="substring-after(@sameAs, '#')"/>
                                        <xsl:value-of select="normalize-space(//*[@xml:id = $id])"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="normalize-space(.)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:text>.</xsl:text>
                </p>
                <xsl:for-each
                    select="//tei:teiHeader[.//tei:publicationStmt/tei:availability/tei:licence]">
                    <xsl:if test="position() = 1">
                        <xsl:for-each select=".//tei:publicationStmt/tei:availability/tei:licence">
                            <p>
                                <xsl:value-of select="normalize-space(.)"/>
                            </p>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:for-each>
                <!-- Pour afficher toutes les différentes licences rédigées : 
                <xsl:for-each-group
                    select="//tei:teiHeader//tei:publicationStmt/tei:availability/tei:licence"
                    group-by=".">
                    <p title="{ancestor::tei:teiHeader[1]//tei:title}">
                        <xsl:value-of select="normalize-space(.)"/>
                    </p>
                </xsl:for-each-group>-->
                <p>
                    <xsl:text>Le projet utilise plusieurs technologies : </xsl:text>
                    <ul>
                        <xsl:for-each select="//tei:teiHeader//tei:appInfo/tei:application/tei:desc">
                            <li>
                                <xsl:apply-templates/>
                            </li>
                        </xsl:for-each>
                    </ul>
                </p>
            </div>
            <xsl:if
                test="//tei:teiHeader//tei:publicationStmt/tei:availability/tei:ab[@type = 'cite_me']">
                <hr/>
                <div>
                    <h3>
                        <xsl:text>Citer le projet</xsl:text>
                    </h3>
                    <div class="ms-1 p-3 mb-2 bg-secondary text-white col-md-8">
                        <xsl:value-of
                            select="normalize-space(//tei:teiHeader//tei:publicationStmt/tei:availability/tei:ab[@type = 'cite_me'])"
                        />
                    </div>
                </div>
            </xsl:if>
            <div>
                <h2>
                    <xsl:text>Accéder aux données</xsl:text>
                </h2>
                <p>
                    <xsl:text>Dans une démarche de science ouverte et de FAIRisation des données des projets
                    accompagnés par l'équipe ELAN, les données propres à chaque projet sont
                    accessibles (sous réserve de respecter les termes de la licence).</xsl:text>
                </p>
                <ol>
                    <li>
                        <xsl:text>Vous pouvez accéder aux schéma d'encodage sous différents formats</xsl:text>
                    </li>
                    <li class="row">
                        <div class="col-md-6">
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:teiHeader//tei:encodingDesc/tei:schemaRef[@type = 'odd']">
                                    <xsl:for-each select="distinct-values(//tei:teiHeader//tei:encodingDesc/tei:schemaRef[@type = 'odd']/@url)">
                                        <a>
                                            <xsl:attribute name="class">
                                                <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="href">
                                                <xsl:value-of
                                                  select="."
                                                />
                                            </xsl:attribute>
                                            <xsl:text>Voir le schéma "</xsl:text>
                                            <code>
                                                <xsl:value-of
                                                    select="replace(replace(replace(., '.*/', ''), '\.odd', ''),'\?ref_type=heads', '')"
                                                />
                                            </code>
                                            <xsl:text>" au format ODD</xsl:text>
                                        </a>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Il n'y a pas de
                                        schéma au format ODD.</span>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//processing-instruction('xml-model')[contains(., 'relax')]">
                                    <xsl:for-each
                                        select="distinct-values(//processing-instruction('xml-model')[contains(., 'relax')]/replace(normalize-space(.), '^.*href=.([^ ]*). .*$', '$1'))">
                                        <a>
                                            <xsl:attribute name="class">
                                                <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="."/>
                                            </xsl:attribute>
                                            <xsl:text>Voir le schéma "</xsl:text>
                                            <code>
                                                <xsl:value-of
                                                  select="replace(replace(., '.*/', ''), '\.rng', '')"
                                                />
                                            </code>
                                            <xsl:text>" au format RNG</xsl:text>
                                        </a>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Il n'y a pas de
                                        schéma au format RNG</span>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:teiHeader//tei:encodingDesc/tei:schemaRef[@type = 'html']">
                                    <xsl:for-each select="distinct-values(//tei:teiHeader//tei:encodingDesc/tei:schemaRef[@type = 'html']/@url)">
                                    <a>
                                        <xsl:attribute name="class">
                                            <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="href">
                                            <xsl:value-of select="."/>
                                        </xsl:attribute>
                                        <xsl:text>Voir le schéma "</xsl:text>
                                        <code>
                                            <xsl:value-of
                                                select="replace(replace(replace(., '.*/', ''), '\.html', ''),'\?ref_type=heads', '')"
                                            />
                                        </code>
                                        <xsl:text>" au format HTML</xsl:text>
                                    </a>
                                    </xsl:for-each>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Il n'y a pas de
                                        manuel d'encodage au format html</span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                        <div class="col-md-6">
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="href">
                                    <xsl:text>https://elan.gricad-pages.univ-grenoble-alpes.fr/schemas-tei/</xsl:text>
                                </xsl:attribute>
                                <xsl:text>Accéder au site de présentation des ODD et des manuels d'encodage
                                des projets accompagnés par l'équipe d’ingénierie ÉLAN.</xsl:text>
                            </a>
                        </div>
                    </li>
                    <div>
                        <xsl:text>Vous pouvez également accéder aux différentes données</xsl:text>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'xslt']//tei:ref[@xml:id = 'link_xsl']">
                                    <a>
                                        <xsl:attribute name="class">
                                            <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="href">
                                            <xsl:value-of
                                                select="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'xslt']//tei:ref[@xml:id = 'link_xsl']/@target"
                                            />
                                        </xsl:attribute>
                                        <xsl:text>Accéder aux XSL</xsl:text>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Vous ne pouvez
                                        pas accéder aux XSL</span>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'xslt']//tei:ref[@xml:id = 'link_xslt_output']">
                                    <a>
                                        <xsl:attribute name="class">
                                            <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="href">
                                            <xsl:value-of
                                                select="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'xslt']//tei:ref[@xml:id = 'link_xslt_output']/@target"
                                            />
                                        </xsl:attribute>
                                        <xsl:text>Accéder aux sorties XSLT</xsl:text>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Vous ne pouvez
                                        pas accéder aux sorties XSLT</span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                        <div class="col-md-6">
                            <xsl:choose>
                                <xsl:when
                                    test="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'gitlab']">
                                    <a>
                                        <xsl:attribute name="class">
                                            <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="href">
                                            <xsl:value-of
                                                select="//tei:teiHeader//tei:appInfo/tei:application[@ident = 'gitlab']//tei:ref/@target"
                                            />
                                        </xsl:attribute>
                                        <xsl:text>Accéder au Code Source</xsl:text>
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <span class="btn btn-outline-warning col-12 m-1">Vous ne pouvez
                                        pas accéder au Code Source</span>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </div>
                </ol>
            </div>
            <div class="row">
                <xsl:if test="//tei:ref[@type = 'doc']">
                    <div class="col-md-6">
                        <h2>
                            <xsl:text>Documentations sur les technologies utilisées</xsl:text>
                        </h2>
                        <xsl:for-each select="//tei:ref[@type = 'doc']">
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="href">
                                    <xsl:value-of select="@target"/>
                                </xsl:attribute>
                                <xsl:value-of select="."/>
                            </a>
                        </xsl:for-each>
                    </div>
                </xsl:if>
                <xsl:choose>
                    <xsl:when
                        test="//tei:teiHeader//tei:publicationStmt/tei:idno[@subtype = 'repository']">
                        <div class="col-md-6 col-md-offset-6">
                            <h2>
                                <xsl:text>Dépôts dans des entrepôts de données</xsl:text>
                            </h2>
                            <p>
                                <xsl:text>Les données du projet sont déposées sur l'entrepôt de confiance Nakala.
                            Les données du projet sont liées à la collection du projet ainsi qu'à la
                            collection ELAN - Litt&amp;Arts.</xsl:text>
                            </p>
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="href">
                                    <xsl:value-of
                                        select="//tei:teiHeader//tei:publicationStmt/tei:idno[@type = 'URI']"
                                    />
                                </xsl:attribute>
                                <img src="logo_nakala.png" height="40" width="40"
                                    alt="logo de l'entrepôt de données Nakala"
                                    onerror="this.src='https://nakala.fr/build/images/nakala.png';"
                                    class="m-1"/>
                                <xsl:text>Lien vers la collection du projet dans Nakala</xsl:text>
                            </a>
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="href">
                                    <xsl:text>https://nakala.fr/collection/10.34847/nkl.b6ea64lr</xsl:text>
                                </xsl:attribute>
                                <img src="logo_nakala.png" height="40" width="40"
                                    alt="logo de l'entrepôt de données Nakala" class="m-1"/>
                                <xsl:text>Lien vers la
                                collection ELAN - Litt&amp;Arts dans Nakala</xsl:text>
                            </a>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <div class="col-md-6 col-md-offset-6">
                            <h2>
                                <xsl:text>Dépôts dans des entrepôts de données</xsl:text>
                            </h2>
                            <p>
                                <xsl:text>Les données du projet seront déposées sur l'entrepôt de confiance Nakala.
                            Les données du projet seront liées à la collection du projet ainsi qu'à la
                            collection ELAN - Litt&amp;Arts.</xsl:text>
                            </p>
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-dark col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="href">
                                    <xsl:text>https://nakala.fr/collection/10.34847/nkl.b6ea64lr</xsl:text>
                                </xsl:attribute>
                                <img src="logo_nakala.png" height="40" width="40"
                                    alt="logo de l'entrepôt de données Nakala"
                                    onerror="this.src='https://nakala.fr/build/images/nakala.png';"
                                    class="m-1"/>Lien
                                vers la collection ELAN - Litt&amp;Arts dans Nakala </a>
                            <a>
                                <xsl:attribute name="class">
                                    <xsl:text>btn btn-outline-warning col-12 m-1</xsl:text>
                                </xsl:attribute>
                                <xsl:text>Une collection du projet sera créée prochainement</xsl:text>
                            </a>
                        </div>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
        <xsl:if test="$stand_alone">
            <xsl:text disable-output-escaping="yes">&lt;/body>
            &lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="@target"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>
</xsl:stylesheet>
